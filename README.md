Platform-specific Config store module for Android
=================================================
This module implements a store for persistent configuration settings
for the Android platform.

Documentation
-------------
This is an early prototype and not much documentation is available
at this point apart from inline comments in the source code.

Building
--------
Building an aar(android archive) file can be done with:
```
./gradlew clean build
```
The build process requires a android-sdk installed, for
convenience the docker image alexjesper/hit-ionic has all requirements
installed.

Publishing
----------
To publish a new aar file to maven.alexandra.dk run:
```
./gradlew clean publish -PrepoUser=$USER_NAME -PrepoPassword=$PASSWORD
```
With $USER_NAME and $PASSWORD set to valid credentails.

Using
-----
Starting the module, and the corresponding platform on a mobile platform
is framework dependent, previously cordova has been used, and a wrapper
project that includes this in the cordova build and starts the module
has been created (https://bitbucket.org/4s/cdvw-config-store)

Issue tracking
--------------
If you encounter bugs or have a feature request, our issue tracker is
available
[here](https://issuetracker4s.atlassian.net/projects/PM/). Please
read our [general 4S
guidelines](http://4s-online.dk/wiki/doku.php?id=process%3Aoverview)
before using it.

License
-------
The source files are released under Apache 2.0, you can obtain a
copy of the License at: http://www.apache.org/licenses/LICENSE-2.0