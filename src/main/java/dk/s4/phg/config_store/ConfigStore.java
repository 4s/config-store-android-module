package dk.s4.phg.config_store;

import java.util.Map;

interface ConfigStore {
    Map<String, String> readValue(String key);
    boolean writeValue(String mapName, Map<String, String> map);
}
