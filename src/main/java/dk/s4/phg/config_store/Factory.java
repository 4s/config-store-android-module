package dk.s4.phg.config_store;

import android.content.Context;

class Factory {
    private Factory() {}

    static ConfigStore getConfigStore(Context context) {
        return new SharedPreferenceConfigStore(context);
    }

    static Validation getValidation() {
        return new Validation();
    }
}
