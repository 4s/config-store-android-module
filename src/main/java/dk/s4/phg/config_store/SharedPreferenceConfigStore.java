package dk.s4.phg.config_store;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Map;
import java.util.stream.Collectors;

class SharedPreferenceConfigStore implements ConfigStore {
    private final Context context;

    SharedPreferenceConfigStore(Context context) {
        this.context = context;
    }

    private SharedPreferences preferences(String mapName) {
        return context.getSharedPreferences(mapName, Context.MODE_PRIVATE);
    }

    @Override
    public Map<String, String> readValue(String key) {
        return preferences(key).getAll().entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, e -> (String) e.getValue()));
    }

    @Override
    public boolean writeValue(String mapName, Map<String, String> map) {
        SharedPreferences.Editor editor = preferences(mapName).edit();
        map.entrySet().forEach(e -> editor.putString(e.getKey(), e.getValue()));
        return editor.commit();
    }
}
