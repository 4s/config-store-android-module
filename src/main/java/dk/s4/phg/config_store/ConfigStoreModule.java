package dk.s4.phg.config_store;

import android.app.Activity;

import dk.s4.phg.baseplate.ApplicationState;
import dk.s4.phg.baseplate.Callback;
import dk.s4.phg.baseplate.Context;
import dk.s4.phg.baseplate.CoreError;
import dk.s4.phg.baseplate.EventImplementation;
import dk.s4.phg.baseplate.FunctionImplementation;
import dk.s4.phg.baseplate.InterfaceEndpoint;
import dk.s4.phg.baseplate.MetaData;
import dk.s4.phg.baseplate.ModuleBase;
import dk.s4.phg.messages.interfaces.config_settings.AnnounceStore;
import dk.s4.phg.messages.interfaces.config_settings.ReadConfiguration;
import dk.s4.phg.messages.interfaces.config_settings.ReadConfigurationSuccess;
import dk.s4.phg.messages.interfaces.config_settings.SolicitStore;
import dk.s4.phg.messages.interfaces.config_settings.WriteConfiguration;
import dk.s4.phg.messages.interfaces.config_settings.WriteConfigurationSuccess;

public class ConfigStoreModule extends ModuleBase {

    private final InterfaceEndpoint endpoint;
    private final ConfigStore configStore;
    private final Validation validation;

    public ConfigStoreModule(Context context, Activity appActivity) {
        super(context, "ConfigStore");

        endpoint = implementsProducerInterface("ConfigSettings");
        configStore = Factory.getConfigStore(appActivity.getApplicationContext());
        validation = Factory.getValidation();

        endpoint.addEventHandler("SolicitStore",
                new EventImplementation<SolicitStore>() {
                    public void accept(SolicitStore args, MetaData meta) {
                        announceStore();
                    }
                });

        endpoint.addFunctionHandler("ReadConfiguration",
                new FunctionImplementation<ReadConfiguration, ReadConfigurationSuccess>() {
                    public void apply(ReadConfiguration args,
                                      Callback<ReadConfigurationSuccess> callback,
                                      MetaData meta) {
                        Validation.Result result = validation.from(args);
                        if (result.isValid()) {
                            callback.success(read(args), false);
                        } else {
                            callback.error(invalidOrMissing(result), false);
                        }
                    }
                });

        endpoint.addFunctionHandler("WriteConfiguration",
                new FunctionImplementation<WriteConfiguration, WriteConfigurationSuccess>() {
                    public void apply(WriteConfiguration args,
                                      Callback<WriteConfigurationSuccess> callback,
                                      MetaData meta) {
                        Validation.Result result = validation.from(args);
                        if (result.isValid()) {
                            if (write(args)) {
                                callback.success(WriteConfigurationSuccess.getDefaultInstance(), false);
                            } else {
                                callback.error(writeFailed(), false);
                            }
                        } else {
                            callback.error(invalidOrMissing(result), false);
                        }
                    }
                });
        start();
    }

    @Override
    protected void onApplicationStateChange(ApplicationState newState) {
        super.onApplicationStateChange(newState);
        if (newState == ApplicationState.RUNNING) {
            announceStore();
        }
    }

    private CoreError invalidOrMissing(Validation.Result result) {
        return endpoint.createError(1, "Invalid or missing argument: " + result.getMessage());
    }

    private CoreError writeFailed() {
        return endpoint.createError(2, "Write to persistence failed");
    }

    private void announceStore() {
        endpoint.postEvent("AnnounceStore", AnnounceStore.getDefaultInstance(), null);
    }

    private ReadConfigurationSuccess read(ReadConfiguration configuration) {
        return MessagesConverter.from(configStore.readValue(configuration.getConfigurationName()));
    }

    private boolean write(WriteConfiguration configuration) {
        return configStore.writeValue(configuration.getConfigurationName(), MessagesConverter.from(configuration));
    }
}
