package dk.s4.phg.config_store;

import java.util.Map;
import java.util.stream.Collectors;

import dk.s4.phg.messages.interfaces.config_settings.KeyValuePair;
import dk.s4.phg.messages.interfaces.config_settings.ReadConfigurationSuccess;
import dk.s4.phg.messages.interfaces.config_settings.WriteConfiguration;

class MessagesConverter {
    private MessagesConverter() {
    }

    static ReadConfigurationSuccess from(Map<String, String> map) {
        ReadConfigurationSuccess.Builder builder = ReadConfigurationSuccess.newBuilder();
        map.entrySet().stream()
                .map(e -> KeyValuePair.newBuilder().setKey(e.getKey()).setValue(e.getValue()).build())
                .forEach(builder::addConfiguration);
        return builder.build();
    }

    static Map<String, String> from(WriteConfiguration configuration) {
        return configuration.getConfigurationList().stream().collect(
                Collectors.toMap(KeyValuePair::getKey, KeyValuePair::getValue));
    }
}
