package dk.s4.phg.config_store;

import java.util.List;

import dk.s4.phg.messages.interfaces.config_settings.KeyValuePair;
import dk.s4.phg.messages.interfaces.config_settings.ReadConfiguration;
import dk.s4.phg.messages.interfaces.config_settings.WriteConfiguration;

class Validation {
    Validation() {
    }

    Result from(WriteConfiguration configuration) {
        if (configuration.getConfigurationName().trim().isEmpty()) {
            return new Result(false, "Configuration name expected");
        }
        List<KeyValuePair> configurations = configuration.getConfigurationList();
        if (configurations.isEmpty()) {
            return new Result(false, "Empty map");
        }
        if (configurations.stream().map(c -> c.getKey()).anyMatch(s -> s.trim().isEmpty())) {
            return new Result(false, "Empty key in map");
        }
        return new Result(true, "");
    }

    Result from(ReadConfiguration configuration) {
        if (configuration.getConfigurationName().trim().isEmpty()) {
            return new Result(false, "Configuration name expected");
        }
        return new Result(true, "");
    }

    class Result {
        private final boolean isValid;
        private final String message;

        private Result(boolean isValid, String message) {
            this.isValid = isValid;
            this.message = message;
        }

        String getMessage() {
            return message;
        }

        boolean isValid() {
            return isValid;
        }
    }
}
