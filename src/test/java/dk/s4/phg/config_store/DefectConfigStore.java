package dk.s4.phg.config_store;

import java.util.HashMap;
import java.util.Map;

class DefectConfigStore implements ConfigStore {

    DefectConfigStore() {
    }

    @Override
    public Map<String, String> readValue(String key) {
        return new HashMap<>();
    }

    @Override
    public boolean writeValue(String mapName, Map<String, String> map) {
        return false;
    }
}

