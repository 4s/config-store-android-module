package dk.s4.phg.config_store;

import org.junit.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import dk.s4.phg.messages.interfaces.config_settings.KeyValuePair;
import dk.s4.phg.messages.interfaces.config_settings.ReadConfigurationSuccess;
import dk.s4.phg.messages.interfaces.config_settings.WriteConfiguration;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.data.MapEntry.entry;

public class MessagesConverterTest {

    @Test
    public void canBuildReadConfigurationSuccess() {
        Map<String, String> map = new HashMap<>();
        map.put("a", "b");
        map.put("a b", "some w space");
        map.put("abc def ghi", "abc");
        map.put("davs", "hello");
        map.put("hest", "æøå horse");

        ReadConfigurationSuccess readConfigurationSuccess = MessagesConverter.from(map);

        assertThat(readConfigurationSuccess.getConfigurationCount()).isEqualTo(5);
        List<KeyValuePair> configuration = readConfigurationSuccess.getConfigurationList();

        for (Map.Entry<String, String> e : map.entrySet()) {
            assertThat(configuration.contains(keyPair(e.getKey(), e.getValue()))).isTrue();
        }
        // Extra sanity check
        assertThat(configuration.contains(keyPair("hest", "æøå horse"))).isTrue();
    }

    @Test
    public void canConvertWriteConfigurationToMap() {
        WriteConfiguration writeConfiguration = WriteConfiguration.newBuilder()
                .setConfigurationName("config")
                .addConfiguration(keyPair("a", "b"))
                .addConfiguration(keyPair("a b", "some w space"))
                .addConfiguration(keyPair("abc def ghi", "abc"))
                .addConfiguration(keyPair("davs", "hello"))
                .addConfiguration(keyPair("hest", "æøå horse"))
                .build();

        Map<String, String> map = MessagesConverter.from(writeConfiguration);

        assertThat(map).containsOnly(
                entry("a", "b"),
                entry("a b", "some w space"),
                entry("abc def ghi", "abc"),
                entry("davs", "hello"),
                entry("hest", "æøå horse"));
    }

    private KeyValuePair keyPair(String key, String value) {
        return KeyValuePair.newBuilder().setKey(key).setValue(value).build();
    }
}