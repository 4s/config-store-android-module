package dk.s4.phg.config_store;

import android.app.Activity;

import com.google.protobuf.MessageLite;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;

import dk.s4.phg.baseplate.ApplicationState;
import dk.s4.phg.baseplate.Callback;
import dk.s4.phg.baseplate.Context;
import dk.s4.phg.baseplate.CoreError;
import dk.s4.phg.baseplate.EventImplementation;
import dk.s4.phg.baseplate.InterfaceEndpoint;
import dk.s4.phg.baseplate.MetaData;
import dk.s4.phg.baseplate.ModuleBase;
import dk.s4.phg.baseplate.Peer;
import dk.s4.phg.baseplate.master.ReflectorContext;
import dk.s4.phg.messages.interfaces.config_settings.AnnounceStore;
import dk.s4.phg.messages.interfaces.config_settings.KeyValuePair;
import dk.s4.phg.messages.interfaces.config_settings.ReadConfiguration;
import dk.s4.phg.messages.interfaces.config_settings.ReadConfigurationSuccess;
import dk.s4.phg.messages.interfaces.config_settings.SolicitStore;
import dk.s4.phg.messages.interfaces.config_settings.WriteConfiguration;
import dk.s4.phg.messages.interfaces.config_settings.WriteConfigurationSuccess;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({Factory.class})
public class ConfigStoreModuleTest {

    private ConfigStoreModule configStoreModule;
    private Context context;

    @Before
    public void setUp() {
        PowerMockito.mockStatic(Factory.class);
        when(Factory.getConfigStore(any())).thenReturn(new MapConfigStore());
        when(Factory.getValidation()).thenReturn(new Validation());
        context = new ReflectorContext(43);
        configStoreModule = new ConfigStoreModule(context, mock(Activity.class));

    }

    @Test
    public void canBeCreated() {
        assertThat(configStoreModule).isNotNull();
        assertThat(configStoreModule.getApplicationState()).isEqualTo(ApplicationState.INITIALIZING);
    }

    @Test
    public void solicitsOnRequest() throws InterruptedException {
        CountDownLatch announceLatch = new CountDownLatch(1);
        AtomicInteger announceCount = new AtomicInteger();
        CallModule callModule = new CallModule(context, "callableModule");
        callModule.registerAnnounceCallback(count -> {
            announceCount.set(count);
            announceLatch.countDown();
        });
        callModule.postEvent("SolicitStore", SolicitStore.getDefaultInstance(), null);
        if (!announceLatch.await(5, TimeUnit.SECONDS)) {
            throw new RuntimeException("Timeout");
        }
        assertThat(announceCount.get()).isEqualTo(1);
    }

    @Test
    public void storePersistenceFails() throws InterruptedException {
        when(Factory.getConfigStore(any())).thenReturn(new DefectConfigStore());
        when(Factory.getValidation()).thenReturn(new Validation());

        configStoreModule = new ConfigStoreModule(context, mock(Activity.class));

        CallModule callModule = new CallModule(context, "callableModule");
        WriteConfiguration configuration = aHorsesConfig();
        CountDownLatch writeErrorLatch = new CountDownLatch(1);
        AtomicReference<CoreError> errorReference = new AtomicReference<>();

        Callback<WriteConfigurationSuccess> callback = new Callback<WriteConfigurationSuccess>() {
            @Override
            public void success(WriteConfigurationSuccess payload, boolean hasMore) {
                System.out.println("Unexpected success");
            }

            @Override
            public void error(CoreError error, boolean hasMore) {
                errorReference.set(error);
                writeErrorLatch.countDown();
            }
        };
        callModule.call("WriteConfiguration", configuration, configStoreModule.getId(), callback);
        await(writeErrorLatch);
        assertThat(errorReference.get().errorCode).isEqualTo(2);
    }

    @Test
    public void storeInvalidNameLessRequest() throws InterruptedException {
        CallModule callModule = new CallModule(context, "callableModule");

        // This is not a valid Config
        WriteConfiguration configuration = WriteConfiguration.newBuilder().build();

        CountDownLatch writeErrorLatch = new CountDownLatch(1);
        AtomicReference<CoreError> errorReference = new AtomicReference<>();

        Callback<WriteConfigurationSuccess> callback = new Callback<WriteConfigurationSuccess>() {
            @Override
            public void success(WriteConfigurationSuccess payload, boolean hasMore) {
                System.out.println("Unexpected success");
            }

            @Override
            public void error(CoreError error, boolean hasMore) {
                errorReference.set(error);
                writeErrorLatch.countDown();
            }
        };
        callModule.call("WriteConfiguration", configuration, configStoreModule.getId(), callback);
        await(writeErrorLatch);
        assertThat(errorReference.get().errorCode).isEqualTo(1);
    }

    @Test
    public void storeInvalidEmptyRequest() throws InterruptedException {
        CallModule callModule = new CallModule(context, "callableModule");

        // This is not a valid Config
        WriteConfiguration configuration = WriteConfiguration.newBuilder().setConfigurationName("name").build();

        CountDownLatch writeErrorLatch = new CountDownLatch(1);
        AtomicReference<CoreError> errorReference = new AtomicReference<>();

        Callback<WriteConfigurationSuccess> callback = new Callback<WriteConfigurationSuccess>() {
            @Override
            public void success(WriteConfigurationSuccess payload, boolean hasMore) {
                System.out.println("Unexpected success");
            }

            @Override
            public void error(CoreError error, boolean hasMore) {
                errorReference.set(error);
                writeErrorLatch.countDown();
            }
        };
        callModule.call("WriteConfiguration", configuration, configStoreModule.getId(), callback);
        await(writeErrorLatch);
        assertThat(errorReference.get().errorCode).isEqualTo(1);
    }

    @Test
    public void readInvalidNameLessRequest() throws InterruptedException {
        CallModule callModule = new CallModule(context, "callableModule");

        // This is not a valid Config
        ReadConfiguration configuration = ReadConfiguration.newBuilder().build();

        CountDownLatch writeErrorLatch = new CountDownLatch(1);
        AtomicReference<CoreError> errorReference = new AtomicReference<>();

        Callback<ReadConfigurationSuccess> callback = new Callback<ReadConfigurationSuccess>() {
            @Override
            public void success(ReadConfigurationSuccess payload, boolean hasMore) {
                System.out.println("Unexpected success");
            }

            @Override
            public void error(CoreError error, boolean hasMore) {
                errorReference.set(error);
                writeErrorLatch.countDown();
            }
        };
        callModule.call("ReadConfiguration", configuration, configStoreModule.getId(), callback);
        await(writeErrorLatch);
        assertThat(errorReference.get().errorCode).isEqualTo(1);
    }

    @Test
    public void storeAndRead() throws InterruptedException {
        CallModule callModule = new CallModule(context, "callableModule");

        {
            // Write this map
            WriteConfiguration configuration = aHorsesConfig();

            CountDownLatch writeSuccessLatch = new CountDownLatch(1);

            Callback<WriteConfigurationSuccess> callback = new Callback<WriteConfigurationSuccess>() {
                @Override
                public void success(WriteConfigurationSuccess payload, boolean lastCall) {
                    writeSuccessLatch.countDown();
                }

                @Override
                public void error(CoreError error, boolean hasMore) {
                    System.out.println("Unexpected error");
                }
            };
            callModule.call("WriteConfiguration", configuration, configStoreModule.getId(), callback);
            await(writeSuccessLatch);
        }

        Map<String, String> result = new HashMap<>();
        {
            // Read the map
            CountDownLatch readSuccessLatch = new CountDownLatch(1);
            Callback<ReadConfigurationSuccess> callback = new Callback<ReadConfigurationSuccess>() {
                @Override
                public void success(ReadConfigurationSuccess payload, boolean lastCall) {
                    payload.getConfigurationList().forEach(e -> result.put(e.getKey(), e.getValue()));
                    readSuccessLatch.countDown();
                }

                @Override
                public void error(CoreError error, boolean hasMore) {
                    System.out.println("Unexpected error");
                }
            };

            callModule.call("ReadConfiguration", ReadConfiguration.newBuilder().setConfigurationName("horses").build(), configStoreModule.getId(), callback);
            await(readSuccessLatch);
        }
        assertThat(result).hasSize(4);
        assertThat(result).containsKeys("Norsk Kaldblodstraver", "Oldenburger");
        assertThat(result.get("Oldenburger")).isEqualTo("4");
    }

    public class CallModule extends ModuleBase {
        private final InterfaceEndpoint endpoint;
        private Consumer<Integer> announceCallback;
        private int storeAnnouncedCount = 0;

        CallModule(Context context, String moduleTag) {
            super(context, moduleTag);
            endpoint = implementsConsumerInterface("ConfigSettings");
            endpoint.addEventHandler("AnnounceStore", new EventImplementation<AnnounceStore>() {
                public void accept(AnnounceStore args, MetaData meta) {
                    announceCallback.accept(++storeAnnouncedCount);
                }
            });
            start();
        }

        void registerAnnounceCallback(Consumer<Integer> consumer) {
            this.announceCallback = consumer;
        }

        <A extends MessageLite, R extends MessageLite> void call(String functionName, A args, Peer receiver, Callback<? super R> callback) {
            endpoint.callFunction(functionName, args, receiver, callback);
        }

        <A extends MessageLite> void postEvent(String functionName, A args, Peer receiver) {
            endpoint.postEvent(functionName, args, receiver);
        }
    }

    private WriteConfiguration aHorsesConfig() {
        return WriteConfiguration.newBuilder()
                .setConfigurationName("horses")
                .addConfiguration(KeyValuePair.newBuilder().setKey("Lipizzaner").setValue("1"))
                .addConfiguration(KeyValuePair.newBuilder().setKey("Norsk Kaldblodstraver").setValue("2"))
                .addConfiguration(KeyValuePair.newBuilder().setKey("Zaniskari").setValue("3"))
                .addConfiguration(KeyValuePair.newBuilder().setKey("Oldenburger").setValue("4"))
                .build();
    }

    private void await(CountDownLatch latch) throws InterruptedException {
        if (!latch.await(160, TimeUnit.SECONDS)) {
            throw new RuntimeException("Timeout");
        }
    }
}
