package dk.s4.phg.config_store;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

class MapConfigStore implements ConfigStore {
    private ConcurrentMap<String, ConcurrentMap<String, String>> map;

    MapConfigStore() {
        this.map = new ConcurrentHashMap<>();
    }

    @Override
    public Map<String, String> readValue(String key) {
        return new HashMap<>(Objects.requireNonNull(map.get(key)));
    }

    @Override
    public boolean writeValue(String mapName, Map<String, String> map) {
        this.map.put(mapName, new ConcurrentHashMap<>(map));
        return true;
    }
}

