package dk.s4.phg.config_store;

import org.junit.Before;
import org.junit.Test;

import dk.s4.phg.messages.interfaces.config_settings.KeyValuePair;
import dk.s4.phg.messages.interfaces.config_settings.ReadConfiguration;
import dk.s4.phg.messages.interfaces.config_settings.WriteConfiguration;

import static org.assertj.core.api.Assertions.assertThat;

public class ValidationTest {
    private Validation validation;

    @Before
    public void setup() {
        validation = new Validation();
    }

    @Test
    public void fromEmptyReadConfig() {
        Validation.Result result = validation.from(ReadConfiguration.newBuilder().build());
        assertThat(result.isValid()).isFalse();
    }

    @Test
    public void fromValidReadConfig() {
        Validation.Result result = validation.from(ReadConfiguration.newBuilder().setConfigurationName("name").build());
        assertThat(result.isValid()).isTrue();
    }

    @Test
    public void fromNameLessWriteConfig() {
        Validation.Result result = validation.from(WriteConfiguration.newBuilder().build());
        assertThat(result.isValid()).isFalse();
    }

    @Test
    public void fromEmptyWriteConfig() {
        Validation.Result result = validation.from(WriteConfiguration.newBuilder().setConfigurationName("davs").build());
        assertThat(result.isValid()).isFalse();
    }

    @Test
    public void fromInvalidKeyWriteConfig() {
        Validation.Result result = validation.from(WriteConfiguration.newBuilder()
                .setConfigurationName("davs")
                .addConfiguration(keyPair("test", "hest"))
                .addConfiguration(keyPair("", "hest"))
                .build());
        assertThat(result.isValid()).isFalse();
    }

    @Test
    public void fromValidKeyWriteConfig() {
        Validation.Result result = validation.from(WriteConfiguration.newBuilder()
                .setConfigurationName("davs")
                .addConfiguration(keyPair("test", "hest"))
                .addConfiguration(keyPair("hest", "test"))
                .build());
        assertThat(result.isValid()).isTrue();
    }

    private KeyValuePair keyPair(String key, String value) {
        return KeyValuePair.newBuilder().setKey(key).setValue(value).build();
    }
}